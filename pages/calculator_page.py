from selenium.webdriver.common.by import By

from base.base_driver import BaseDriver

from utilities.utils import Utils


class CalculatorPage(BaseDriver):
    log = Utils.custom_logger()

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Test build elements
    EXTERNAL_IFRAME = 0
    INNER_IFRAME = "myFrame"
    COMPUTE_ENGINE_BTN = '//md-tab-item[@tabindex="0"]'
    INSTANCES = '//input[@id="input_98"]'
    OSSOFTWARE__DD = '//md-select-value[@id="select_value_label_90"]'
    OS_SOFTWARE_VALUES = '//div[@id="select_container_112"]//md-option'
    SERIES = '//md-select-value[@id="select_value_label_93"]'
    SERIES_VALUES = '//div[@id="select_container_124"]//md-option'
    MACHINE_TYPE = '//md-select-value[@id="select_value_label_94"]'
    MACHINE_TYPE_VALUES = '//div[@id="select_container_126"]//md-option'
    ADD_GPUS_CHECKBOX = '//md-checkbox[@aria-label="Add GPUs"]'
    GPUS_TYPE = '//md-select[@aria-label="GPU type"]'
    GPUS_TYPE_VALUES = '//div[@id="select_container_506"]//md-option'
    NUMBER_OF_GPUS = '//md-select[@id="select_507"]'
    NUMBER_OF_GPUS_VALUES = '//div[@id="select_container_508"]//md-option'
    SSD = '//md-select[@id="select_464"]'
    SSD_VALUES = '//div[@id="select_container_465"]//md-option'
    DATACENTER = '//md-select[@id="select_131"]'
    DATACENTER_VALUES = '//div[@id="select_container_132"]//md-option'
    COMMITTED_USAGE = '//md-select[@id="select_138"]'
    COMMITTED_USAGE_VALUES = '//div[@id="select_container_139"]//md-option'
    ESTIMATE_BTN = '//button[contains(text(),"Add to Estimate")]'
    EMAIL_ESTIMATE = '//button[@id="Email Estimate"]'
    EMAIL_FIELD = '//form[@name="emailForm"]//div[3]//input'
    SEND_EMAIL_BTN = '//button[contains(text(),"Send Email")]'

    # Assertions elements
    ESTIMATE_SUM = '//md-card-content[@id="resultBlock"]//h2[2]'
    ESTIMATE_INSTANCES = '//div[@aria-level="2"]//span'
    ESTIMATE_REGION = '//div[contains(text(), "Region:")]'
    ESTIMATE_HOURS = '//div[contains(text(), "total hours per month")]'
    ESTIMATE_TERM = '//div[contains(text(), "Commitment term:")]'
    ESTIMATE_PROV_MODEL = '//div[contains(text(), "Provisioning model:")]'
    ESTIMATE_INSTANCE_TYPE = '//div[contains(text(), "Instance type:")]'
    ESTIMATE_BINDING_SUMS = '//div[@class="ng-binding"]'
    ESTIMATE_OS_SOFTWARE = (
        '//div[contains(text(), "Operating System / Software:")]//span'
    )
    ESTIMATE_SSD = '//div[contains(text(), "Local SSD:")]'
    ESTIMATE_TOTAL = "//h2/b"

    # Iframe handling
    def switch_to_frame(self, iframe):
        self.driver.switch_to.frame(iframe)

    def enter_both_iframes(self, ext_iframe, int_iframe):
        self.switch_to_frame(ext_iframe)
        self.log.info("Enter into external iframe.")
        self.switch_to_frame(int_iframe)
        self.log.info("Enter into inner iframe.")

    # TEST BUILD METHODS
    #  Compute engine
    def get_compute_engine_btn(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.COMPUTE_ENGINE_BTN)

    def click_on_compute_engine_btn(self):
        self.get_compute_engine_btn().click()
        self.log.info("Click on the Compute Engine btn.")

    # Instances
    def get_instances(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.INSTANCES)

    def set_instances_value(self, value):
        self.get_instances().click()
        self.get_instances().send_keys(value)
        self.log.info("Enter instances value.")

    # OS/Softvare
    def get_os_software(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.OSSOFTWARE__DD)

    def set_os_software(self, os_software):
        self.get_os_software().click()
        os_software_list = self.wait_for_presence_of_all_elements(
            By.XPATH, self.OS_SOFTWARE_VALUES
        )
        Utils.select_item_from_list(os_software_list, os_software)
        self.log.info("Select Operating System/Software.")

    # Series
    def get_series(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.SERIES)

    def set_series(self, series):
        self.get_series().click()
        series_list = self.wait_for_presence_of_all_elements(
            By.XPATH, self.SERIES_VALUES
        )
        Utils.select_item_from_list(series_list, series)
        self.log.info("Select Series.")

    # Machine Type
    def get_machine_type(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.MACHINE_TYPE)

    def set_machine_type(self, machine_type):
        self.get_machine_type().click()
        machine_types_list = self.wait_for_presence_of_all_elements(
            By.XPATH, self.MACHINE_TYPE_VALUES
        )
        Utils.select_item_from_list(machine_types_list, machine_type)
        self.log.info("Select Machine type.")

    # GPUs
    def get_add_gpus_checkbox(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.ADD_GPUS_CHECKBOX)

    def get_gpus_type_select(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.GPUS_TYPE)

    def get_gpus_type_values(self):
        return self.wait_for_presence_of_all_elements(By.XPATH, self.GPUS_TYPE_VALUES)

    def get_gpus_num_select(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.NUMBER_OF_GPUS)

    def get_gpus_num_values(self):
        return self.wait_for_presence_of_all_elements(
            By.XPATH, self.NUMBER_OF_GPUS_VALUES
        )

    def add_gpus_config(self, gpus_type, num_gpus):
        self.get_add_gpus_checkbox().click()
        self.log.info("Turn on GPUs checkbox.")

        self.get_gpus_type_select().click()
        gpus_type_list = self.get_gpus_type_values()
        Utils.select_item_from_list(gpus_type_list, gpus_type)
        self.log.info("Select GPUs type.")

        self.get_gpus_num_select().click()
        num_gpus_list = self.get_gpus_num_values()
        Utils.select_item_from_list(num_gpus_list, num_gpus)
        self.log.info("Select GPUs number.")

    # SSD
    def get_ssd(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.SSD)

    def get_ssd_values(self):
        return self.wait_for_presence_of_all_elements(By.XPATH, self.SSD_VALUES)

    def ssd_config(self, num_ssd):
        self.get_ssd().click()
        ssd_conf_list = self.get_ssd_values()
        Utils.select_item_from_list(ssd_conf_list, num_ssd)
        self.log.info("Select SSD number.")

    # Datacenter
    def get_datacenter(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.DATACENTER)

    def get_datacenter_values(self):
        return self.wait_for_presence_of_all_elements(By.XPATH, self.DATACENTER_VALUES)

    def datacenter_config(self, location):
        self.get_datacenter().click()
        dc_conf_list = self.get_datacenter_values()
        Utils.select_item_from_list(dc_conf_list, location)
        self.log.info("Select Datacenter location.")

    # Commited usage
    def get_com_usage(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.COMMITTED_USAGE)

    def get_com_usage_values(self):
        return self.wait_for_presence_of_all_elements(
            By.XPATH, self.COMMITTED_USAGE_VALUES
        )

    def com_usage_config(self, com_usage):
        self.get_com_usage().click()
        com_usage_list = self.get_com_usage_values()
        Utils.select_item_from_list(com_usage_list, com_usage)
        self.log.info("Select Committed usage.")

    # Estimate btn
    def get_estimate(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.ESTIMATE_BTN)

    def estimate_btn_click(self):
        self.get_estimate().click()
        self.log.info("Click on Estimate btn.")

    # Email estimate
    def get_email_estimate_btn(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.EMAIL_ESTIMATE)

    def email_estimate_btn_click(self):
        self.get_email_estimate_btn().click()
        self.log.info("Click on Email estimate.")

    # Filling the form with previous methods  (WITH SENDING)
    def fill_the_form_with_sending(
        self,
        instances,
        os_software,
        series,
        machine_type,
        gpus_type,
        num_gpus,
        num_ssd,
        location,
        com_usage,
    ):
        self.enter_both_iframes(self.EXTERNAL_IFRAME, self.INNER_IFRAME)
        self.click_on_compute_engine_btn()
        self.set_instances_value(instances)
        self.set_os_software(os_software)
        self.set_series(series)
        self.set_machine_type(machine_type)
        self.add_gpus_config(gpus_type, num_gpus)
        self.ssd_config(num_ssd)
        self.datacenter_config(location)
        self.com_usage_config(com_usage)
        self.estimate_btn_click()
        self.email_estimate_btn_click()

    # Filling the form with previous methods (WITHOUR SENDING)
    def fill_the_form_without_sending(
        self,
        instances,
        os_software,
        series,
        machine_type,
        gpus_type,
        num_gpus,
        num_ssd,
        location,
        com_usage,
    ):
        self.enter_both_iframes(self.EXTERNAL_IFRAME, self.INNER_IFRAME)
        self.click_on_compute_engine_btn()
        self.set_instances_value(instances)
        self.set_os_software(os_software)
        self.set_series(series)
        self.set_machine_type(machine_type)
        self.add_gpus_config(gpus_type, num_gpus)
        self.ssd_config(num_ssd)
        self.datacenter_config(location)
        self.com_usage_config(com_usage)
        self.estimate_btn_click()

    # Email field
    def get_email_field(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.EMAIL_FIELD)

    def set_email_field(self, email):
        self.get_email_field().click()
        self.get_email_field().send_keys(email)
        self.log.info("Set Email field value.")

    # Send email btn
    def get_send_email_btn(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.SEND_EMAIL_BTN)

    def send_email_btn_click(self):
        self.get_send_email_btn().click()
        self.log.info("Click on Send Email btn.")

    # Send email with the cost calculation
    def send_email(self, email):
        self.enter_both_iframes(self.EXTERNAL_IFRAME, self.INNER_IFRAME)
        self.set_email_field(email)
        self.send_email_btn_click()
        self.log.info("Email successfuly send.")

    # ASSERTIONS METHODS
    def get_estimate_sum(self):
        sum = self.wait_until_presence_of_element_located(By.XPATH, self.ESTIMATE_SUM)
        self.log.warning("Get sum for assertion.")
        return sum

    def get_estimate_instances(self):
        instances = self.wait_until_presence_of_element_located(
            By.XPATH, self.ESTIMATE_INSTANCES
        )
        self.log.warning("Get instances for assertion.")
        return instances

    def get_estimate_region(self):
        region = self.wait_until_presence_of_element_located(
            By.XPATH, self.ESTIMATE_REGION
        )
        self.log.warning("Get region for assertion.")
        return region

    def get_estimate_hours(self):
        hours = self.wait_until_presence_of_element_located(
            By.XPATH, self.ESTIMATE_HOURS
        )
        self.log.warning("Get hours for assertion.")
        return hours

    def get_estimate_term(self):
        term = self.wait_until_presence_of_element_located(By.XPATH, self.ESTIMATE_TERM)
        self.log.warning("Get term for assertion.")
        return term

    def get_estimate_prov_model(self):
        prov_model = self.wait_until_presence_of_element_located(
            By.XPATH, self.ESTIMATE_PROV_MODEL
        )
        self.log.warning("Get provision model for assertion.")
        return prov_model

    def get_estimate_instance_type(self):
        instance_type = self.wait_until_presence_of_element_located(
            By.XPATH, self.ESTIMATE_INSTANCE_TYPE
        )
        self.log.warning("Get instances type for assertion.")
        return instance_type

    def get_estimate_binding_sums(self):
        binding_sums = self.wait_for_presence_of_all_elements(
            By.XPATH, self.ESTIMATE_BINDING_SUMS
        )
        self.log.warning("Get binding sums for assertion.")
        return binding_sums

    def get_estimate_os_software(self):
        os_soft = self.wait_until_presence_of_element_located(
            By.XPATH, self.ESTIMATE_OS_SOFTWARE
        )
        self.log.warning("Get os/software for assertion.")
        return os_soft

    def get_estimate_ssd(self):
        ssd = self.wait_until_presence_of_element_located(By.XPATH, self.ESTIMATE_SSD)
        self.log.warning("Get SSD for assertion.")
        return ssd

    def get_estimate_total(self):
        total = self.wait_until_presence_of_element_located(
            By.XPATH, self.ESTIMATE_TOTAL
        )
        self.log.warning("Get estimate total for assertion.")
        return total
