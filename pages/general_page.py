from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from base.base_driver import BaseDriver

from utilities.utils import Utils


class GeneralPage(BaseDriver):
    log = Utils.custom_logger()

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    SEARCH_FIELD = "//input[@placeholder='Search']"

    def get_search_field(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.SEARCH_FIELD)

    def click_on_search_field(self):
        self.get_search_field().click()

    def enter_search_value(self, searched_value):
        self.get_search_field().send_keys(searched_value)
        self.wait_until_text_to_be_present_in_element_value(By.XPATH, self.SEARCH_FIELD, searched_value)
        self.get_search_field().send_keys(Keys.ENTER)

    def find_calculator(self, searched_value):
        self.click_on_search_field()
        self.enter_search_value(searched_value)
        self.log.info("Search google cloud calculator.")
