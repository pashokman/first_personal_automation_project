from selenium.webdriver.common.by import By

from base.base_driver import BaseDriver

from utilities.utils import Utils


class MailPage(BaseDriver):
    log = Utils.custom_logger()

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    EMAIL = '//span[@id="email"]'
    MAIL_URL = "https://www.minuteinbox.com/"
    LETTER = '//tbody//tr[1]//td[2][contains(text(),"Google Cloud Price Estimate")]'
    IFRAME = "iframeMail"
    SUM = "//td[2]//h3"

    # Page
    def open_mail_page(self):
        self.driver.execute_script(f"""window.open("{self.MAIL_URL}","_blank");""")

    def get_email_address(self):
        email_field = self.wait_until_presence_of_element_located(By.XPATH, self.EMAIL)
        return email_field.text

    def work_with_male_page(self):
        self.wait_until_next_page_open()
        email = self.get_email_address()
        self.log.info("Copy email.")
        return email

    # Letter
    def get_letter(self):
        return self.wait_until_presence_of_element_located(By.XPATH, self.LETTER)

    def letter_click(self):
        self.get_letter().click()
        self.log.info("Click on letter.")

    def open_letter(self):
        self.letter_click()
        self.log.info("Letter is opened.")

    # Iframe handling
    def switch_to_frame(self, iframe):
        self.driver.switch_to.frame(iframe)

    def eneter_letter_iframe(self, iframe):
        self.switch_to_frame(iframe)
        self.log.info("Enter into letter iframe.")

    # Sum
    def get_sum(self):
        sum = self.wait_until_presence_of_element_located(By.XPATH, self.SUM)
        self.log.info("Get sum to assert.")
        return sum

    def get_sum_in_frame(self):
        self.eneter_letter_iframe(self.IFRAME)
        return self.get_sum()
