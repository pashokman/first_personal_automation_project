import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from base.base_driver import BaseDriver

from utilities.utils import Utils
import logging


class SearchResultsPage(BaseDriver):
    log = Utils.custom_logger()

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    SEARCH_RESULT = "//div[@class='gs-title']//b[contains(text(),'Google Cloud Pricing Calculator')]"

    def get_search_result(self):
        return self.wait_until_element_is_clicable(By.XPATH, self.SEARCH_RESULT)
    
    def click_on_search_result(self):
        self.get_search_result().click()
        self.log.info("Select needed search result.")