from selenium import webdriver
import pytest
import os

driver = None


@pytest.fixture(autouse=True)
def setup(request, browser, url):
    global driver
    if browser == "chrome":
        # Launch the browser and open the travel website
        option = webdriver.ChromeOptions()
        # hide string "Chrome is being controled by automated software"
        option.add_experimental_option("excludeSwitches", ["enable-automation"])
        # Pass the argument 1 to allow and 2 to block notifications
        option.add_experimental_option(
            "prefs", {"profile.default_content_setting_values.notifications": 2}
        )
        driver = webdriver.Chrome(options=option)
    elif browser == "firefox":
        driver = webdriver.Firefox()
    elif browser == "edge":
        driver = webdriver.Edge()
    driver.maximize_window()
    driver.get(url)
    request.cls.driver = driver
    yield
    driver.quit()


def pytest_addoption(parser):
    parser.addoption("--browser1")
    parser.addoption("--url1")


@pytest.fixture(scope="class", autouse=True)
def browser(request):
    return request.config.getoption("--browser1")


@pytest.fixture(scope="class", autouse=True)
def url(request):
    return request.config.getoption("--url1")


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item):
    pytest_html = item.config.pluginmanager.getplugin("html")
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, "extra", [])
    if report.when == "call":
        # always add url to report
        extra.append(pytest_html.extras.url(driver.current_url))
        xfail = hasattr(report, "wasxfail")
        if (report.skipped and xfail) or (report.failed and not xfail):
            # only add additional html on failure
            report_directory = os.path.dirname(item.config.option.htmlpath)
            # file_name = str(int(round(time.time() * 1000))) + ".png"    # make name of the screenshot like timestamp
            file_name = (
                report.nodeid.replace("::", "_") + ".png"
            )  # make name of the screenshot like test name
            # also need to create new "testcase" folder in reports folder to save there screenshots
            destinationFile = os.path.join(report_directory, file_name)
            driver.save_screenshot(destinationFile)
            if file_name:
                html = (
                    '<div><img src="%s" alt="screenshot" style="width:300px;height=200px" '
                    'onclick="window.open(this.src)" align="right"/></div>' % file_name
                )
            extra.append(pytest_html.extras.html(html))
        report.extra = extra


def pytest_html_report_title(report):
    report.title = "My test automation report"
