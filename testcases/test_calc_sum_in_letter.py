import pytest
from pages.general_page import GeneralPage
from pages.search_results_page import SearchResultsPage
from pages.calculator_page import CalculatorPage
from pages.mail_page import MailPage
from utilities.utils import Utils
import softest
import logging
from ddt import ddt, data, unpack, file_data


@pytest.mark.usefixtures("setup")
@ddt
class TestSearchgAndVerifySumInLetter(softest.TestCase):
    log = Utils.custom_logger(log_level=logging.INFO)

    @pytest.fixture(autouse=True)
    def class_setup(self):
        self.gp = GeneralPage(self.driver)
        self.sp = SearchResultsPage(self.driver)
        self.cp = CalculatorPage(self.driver)
        self.ut = Utils()
        self.mp = MailPage(self.driver)

    @data(
        *Utils.read_data_from_csv(
            "../first_auto_framework_python/testdata/tdatacsv.csv"
        )
    )
    @unpack
    def test_cloud_service_price(
        self,
        searched_value,
        instances,
        os_software,
        series,
        machine_type,
        gpus_type,
        num_gpus,
        num_ssd,
        location,
        com_usage,
        sum,
    ):
        self.log.critical("      ---     START OF TEST - Calc sum in letter      ---     ")

        self.gp.find_calculator(searched_value)

        self.sp.click_on_search_result()

        self.cp.fill_the_form_with_sending(
            instances,
            os_software,
            series,
            machine_type,
            gpus_type,
            num_gpus,
            num_ssd,
            location,
            com_usage,
        )

        main_window_handler = self.driver.current_window_handle

        self.mp.open_mail_page()
        for window_handle in self.driver.window_handles:
            if window_handle != main_window_handler:
                self.driver.switch_to.window(window_handle)
                break
        mail_window_handler = self.driver.current_window_handle
        email = self.mp.work_with_male_page()

        self.driver.switch_to.window(main_window_handler)
        self.cp.send_email(email)

        self.driver.switch_to.window(mail_window_handler)
        self.mp.open_letter()
        total = self.mp.get_sum_in_frame()
        assert total.text == f"USD {sum}", "The sum in the letter is NOT right."

        self.log.critical("      ---     SUCCESSFUL END OF TESTING - Calc sum in letter     ---     ")
