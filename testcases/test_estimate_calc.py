import pytest
from pages.general_page import GeneralPage
from pages.search_results_page import SearchResultsPage
from pages.calculator_page import CalculatorPage
from pages.mail_page import MailPage
from utilities.utils import Utils
import softest
import logging
from ddt import ddt, data, unpack, file_data


@pytest.mark.usefixtures("setup")
@ddt
class TestSearchgAndVerifyEstimateFormData(softest.TestCase):
    log = Utils.custom_logger(log_level=logging.INFO)

    @pytest.fixture(autouse=True)
    def class_setup(self):
        self.gp = GeneralPage(self.driver)
        self.sp = SearchResultsPage(self.driver)
        self.cp = CalculatorPage(self.driver)
        self.ut = Utils()
        self.mp = MailPage(self.driver)

    @data(
        *Utils.read_data_from_csv(
            "../first_auto_framework_python/testdata/tdatacsv.csv"
        )
    )
    @unpack
    def test_cloud_service_price(
        self,
        searched_value,
        instances,
        os_software,
        series,
        machine_type,
        gpus_type,
        num_gpus,
        num_ssd,
        location,
        com_usage,
        sum,
    ):
        self.log.critical(
            "      ---     START OF TESTING - estimate of calculator     ---     "
        )

        self.gp.find_calculator(searched_value)

        self.sp.click_on_search_result()

        self.cp.fill_the_form_without_sending(
            instances,
            os_software,
            series,
            machine_type,
            gpus_type,
            num_gpus,
            num_ssd,
            location,
            com_usage,
        )

        estimate_sum = self.cp.get_estimate_sum()
        assert (
            estimate_sum.text == f"{sum} / mo"
        ), "The sum in Estimate form is NOT right."

        estimate_instances = self.cp.get_estimate_instances()
        assert (
            estimate_instances.text == f"{instances} x"
        ), "The instances value in Estimate form is NOT right."

        estimate_region = self.cp.get_estimate_region()
        assert (
            estimate_region.text[8:] == "Frankfurt"
        ), "The regiona in Estimate form is NOT right."

        estimate_hours = self.cp.get_estimate_hours()
        assert (
            estimate_hours.text == "2,920 total hours per month"
        ), "The hours in Estimate form is NOT right."

        estimate_term = self.cp.get_estimate_term()
        assert (
            estimate_term.text[17:] == "1 Year"
        ), "The Commitment term in Estimate form is NOT right."

        estimate_prov_model = self.cp.get_estimate_prov_model()
        assert (
            estimate_prov_model.text[20:] == "Regular"
        ), "The Provisioning model in Estimate form is NOT right."

        estimate_instance_type = self.cp.get_estimate_instance_type()
        assert (
            "n1-standard-8" in estimate_instance_type.text
        ), "The Instance type in Estimate form is NOT right."

        estimate_binding_sums = self.cp.get_estimate_binding_sums()
        assert (
            estimate_binding_sums[0].text == "USD 899.76"
        ), "The Instance type Sum in Estimate form is NOT right."

        os_soft = self.cp.get_estimate_os_software()
        assert (
            os_soft.text == "Free"
        ), "The OS/Software in Estimate form is NOT right."

        ssd = self.cp.get_estimate_ssd()
        assert (
            f"{num_ssd}x375 GiB" in ssd.text
        ), "The Local SSD in Estimate form is NOT right."

        assert (
            estimate_binding_sums[1].text == "USD 181.44"
        ), "The Instance type Sum in Estimate form is NOT right."

        total = self.cp.get_estimate_total()
        assert (
            total.text == f"Total Estimated Cost: USD {sum} per 1 month"
        ), "The Total Estimate in Estimate form is NOT right."

        self.log.critical(
            "      ---     SUCCESSFUL END OF TESTING - estimate of calculator      ---     "
        )
